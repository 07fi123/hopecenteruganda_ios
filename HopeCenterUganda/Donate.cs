using Foundation;
using CoreGraphics;
using System;
using UIKit;
using Firebase.Database;


namespace HopeCenterUganda
{


    public partial class Donate : UIView
    {
		public UITextView textview;

        public Donate(IntPtr handle) : base(handle)
        {

            /* Setup for ui: We will need to create all the required elements 
             * and buttions. Also we must have all of the dynamic ui elements 
             * templates setup before we have that working. It might be a nice time 
             * to figure out animations and test out elements launching pages and things.
             *  ETA on this page is End of July.. 
             * 
             *  OH SHIT AUTO LAYOUT STRIKES ONCE MORE !!!!!
             *  It looks like we may need to push back our feature
             *  Complete date for this page ... I need to do a lot of 
             *  learning on this guy
             */


            //Header Code
			UIView Header = new UIView(new CGRect(0, 0, Frame.Width, 95));

			Header.BackgroundColor = UIColor.White;

			UIImageView HopeCenterLogo = new UIImageView(UIImage.FromBundle("Logo"));
			Header.AddSubview(HopeCenterLogo);
            AddSubview(Header);
			Header.ConstrainLayout(() => HopeCenterLogo.Frame.GetCenterX() == Header.Frame.GetCenterX() &&
								   HopeCenterLogo.Frame.Bottom >= Header.Frame.Bottom - 5 &&
								   HopeCenterLogo.Frame.Top == Header.Frame.Top + 20);

            this.ConstrainLayout(() => Header.Frame.Left == Frame.Left && Header.Frame.Right == Frame.Right && Header.Frame.Top == Frame.Top);

            //End Header



            //Static Content 


            ///SUPPORT 
            /// OK so this bit of code has goten way out of hand.. Im haveing to re think all of what
            /// I know about Layouts and such. I am wondering how I can setup this thing so It has a size
            /// but Is not limited to it.. Its something I must solve but It may not be now
            /// Might need to start a feature list that isnt in my head 
            /// Lots of things to do in a sort amount of time.
            UIView Support = new UIView(){
				BackgroundColor = UIColor.FromRGB(23, 126, 117),

			};
           Support.Scale();

			UILabel SupportLabel = new UILabel()
            {
                Text = "Support God's Work",
                TextColor = UIColor.White,
                AdjustsFontSizeToFitWidth = true
            };
            Support.Layer.CornerRadius = 3;
            Support.Layer.MasksToBounds = true;

            SupportLabel.MinimumScaleFactor = 10;
            Support.AddSubview(SupportLabel);
            Support.BringSubviewToFront(SupportLabel);
            AddSubview(Support);

            this.ConstrainLayout(() => Support.Frame.Left == Frame.Left + 20 && Support.Frame.Right == Frame.Right - 20 && Support.Frame.Top == Header.Frame.Bottom + 20);



			//End Static Content

			//Stack View
			UIStackView DonateStack = new UIStackView();
			//DonateStack.BackgroundColor = UIColor.Orange;
			DonateStack.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
			DonateStack.AutosizesSubviews = true;
			DonateStack.Axis = UILayoutConstraintAxis.Vertical;
			DonateStack.Alignment = UIStackViewAlignment.Center;
			DonateStack.Distribution = UIStackViewDistribution.Fill;
			DonateStack.Spacing = 2;
			AddSubview(DonateStack);


            this.ConstrainLayout(() => DonateStack.Frame.Top == Support.Frame.Bottom && DonateStack.Frame.Bottom == Frame.Bottom);


            textview = new UITextView();
			textview.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
			Console.WriteLine(textview.Frame.Bottom);
			Console.WriteLine(textview.Frame.Top);
			textview.ScrollEnabled = true;
			textview.BackgroundColor = UIColor.White;
            DonateStack.AddArrangedSubview(textview);

            this.ConstrainLayout(() => textview.Frame.Left == Frame.Left + 20 && textview.Frame.Right == Frame.Right - 20 );



			// Database Update code , this is majjic 
			DatabaseReference rootNode = Database.DefaultInstance.GetRootReference();

            DatabaseReference mastersheet = rootNode.GetChild(0.ToString());
            rootNode.ObserveEvent(DataEventType.Value, (snapshot) =>{

				var folderData = snapshot.GetValue<NSDictionary>();

                Console.WriteLine(folderData.ValueForKey((NSString)"0"));

                textview.Text = folderData.ToString();

			},(error) => {
				Console.WriteLine(error.LocalizedDescription);
			});

            //DonateStack.Frame = new CGRect(new CGPoint(10, 100), new CGSize(DonateStack.Frame.Width, 600));


	    	}

	
        }
    }
